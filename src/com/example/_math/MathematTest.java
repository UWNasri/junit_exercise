package com.example._math;


import static org.junit.Assert.*;


import org.junit.Test;

public class MathematTest {

	@Test
	public void testSin() 
	{
		//fail("Not yet implemented");
		
		double expected;
		double actual;
		double rad;
		double degree;
		
		//Checking for Radian values
		// floating negative and positive points
		rad = 3.14;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);

		rad = -1.83;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		
		rad = 10.25;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		rad = 89.99;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		rad = 164.01;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		
		
		rad = -2.4;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		rad = 5.5;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		rad = -8.003;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);

		//signed integer values point
		rad = 2;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		rad = 1;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		rad = 10;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		rad = -3;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		
		
		// edge points
		rad = -0;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		rad = 0;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		

		
		
		rad = 1;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		rad = 3.14159265358979323846;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		 
		
		//Checking for degree values
		// floating negative and positive points
		degree = 2.99;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
		degree = -1.57;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
		degree = 0.02;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
		//signed integer values point
		degree = 30;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
				
	    rad = -1;
		expected= Math.sin(rad);
		actual = Mathemat.sin(rad, false);
		assertEquals(rad + " Sin of Radian: ", expected, actual, 0.001);
		
		degree = 60;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
		//edge points
		degree = -0;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
		degree = -90;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);
				
		degree = 180;
		expected= Math.sin(Math.toRadians(degree));
		actual = Mathemat.sin(degree, true);
		assertEquals(degree + " Sin of Degree: ", expected, actual, 0.001);

	}
	
	@Test
	public void testCos() {
		
		double expected;
		double actual;
		double rad;
		double degree;
		
		//Checking for Radian values
		// floating negative and positive points
		rad = 3.14;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);

		rad = -1.83;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		rad = -8.003;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);

		//signed integer values point
		rad = 2;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		rad = 10;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		rad = -3;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		
		
		// edge points
		rad = -0;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		rad = 0;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		rad = 1;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		
		rad = 3.14159265358979323846;
		expected= Math.cos(rad);
		actual = Mathemat.cos(rad, false);
		assertEquals(rad + " Cos of Radian: ", expected, actual, 0.001);
		 
		
		//Checking for degree values
		// floating negative and positive points
		degree = 2.99;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		degree = -1.57;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		degree = 0.02;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		//signed integer values point
		degree = 30;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		degree = 60;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		//edge points
		degree = -0;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		degree = -90;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
				
		degree = 180;
		expected= Math.cos(Math.toRadians(degree));
		actual = Mathemat.cos(degree, true);
		assertEquals(degree + " Cos of Degree: ", expected, actual, 0.001);
		
		
	} 

	@Test
	public void testTan() {
		double expected;
		double actual;
		double rad;
		double degree;
		
		//Checking for Radian values
		// floating negative and positive points
		rad = 3.14;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);

		rad = -1.83;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		rad = -8.003;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);

		//signed integer values point
		rad = 2;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		rad = 10;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		rad = -3;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		
		
		// edge points
		rad = -0;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		rad = 0;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		rad = 1;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		
		rad = 3.14159265358979323846;
		expected= Math.tan(rad);
		actual = Mathemat.tan(rad, false);
		assertEquals(rad + " Tang of Radian: ", expected, actual, 0.001);
		 
		
		//Checking for degree values
		// floating negative and positive points
		degree = 2.99;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
				
		degree = -1.57;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
				
		degree = 0.02;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
				
		//signed integer values point
		degree = 30;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
				
		degree = 60;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
				
		//edge points
		degree = -0;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
				
				
		degree = 180;
		expected= Math.tan(Math.toRadians(degree));
		actual = Mathemat.tan(degree, true);
		assertEquals(degree + " Tang of Degree: ", expected, actual, 0.001);
		
	}

}
