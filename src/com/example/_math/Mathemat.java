
package com.example._math;
/**
 * @author Group 20
 *	This class is a substitute of the Math class in java util library which only implements Sin, Cos, Tan, and toRadian functions
 */
public final class Mathemat extends Exception{
	public static final double PI = 3.14159265358979323846;

	/**
	 * @param x is either radian or degree from which we want to use sin function on
	 * @param is_degree determines whether x is given in radian or degree
	 * @return returns the value of sin function given the parameters
	 */
	public static double sin(double x, boolean is_degree)
	{
		try {
			if(x >= 6.28319 && is_degree==false) {
				x=x%6.28319;
			}
			if(x <= (-6.28319) && is_degree==false) {
				x=x%(-6.28319);
			}
			if(is_degree == true) {
				x = toRadian(x);
			}
			int n;
			double val=0;
			for (n=0;n<8;n++)
			{
				double p = power(-1,n);
				double px = power(x,2*n+1);
				long fac = factorial(2*n+1);
				val += p * px / fac;

			}
			return val;
		}
		catch(ArithmeticException e) {
			System.out.println("The Value Entered is Incorrect");
			System.out.println("**************************************");
			e.printStackTrace();
		}

		return 0;
	}
	/**
	 * @param x is the number we want to perform power option on
	 * @param n is the number we want x to be the power of
	 * @return the value of x to the power of n
	 */
	static double power(double x,int n)
	{
		double val=1;
		int i;
		for (i=1;i<=n;i++)
		{
			val*=x;
		}
		return val;
	}
	/**
	 * @param m the number which we want to perform factorial operation on
	 * @return the value of m!(m factorial)
	 */
	static long factorial(int m)
	{
		if (m==0 || m==1) {
			return 1;
		}
		else  {
			return m*factorial(m-1);
		}

	}
	/**
	 * @param x is in degrees which we want to turn into radian
	 * @return returns the radian value of x
	 */
	public static double toRadian(double x) {
		x = x*PI/180;
		return x;
	}
	/**
	 * @param x is either radian or degree from which we want to use cos function on
	 * @param is_degree determines whether x is given in radian or degree
	 * @return returns the value of cos function given the parameters
	 */
	public static double cos(double x, boolean is_degree) {
		try {
			if(x >= 6.28319 && is_degree==false) {
				x=x%6.28319;
			}
			if(x <= (-6.28319) && is_degree==false) {
				x=x%(-6.28319);
			}
			if(is_degree == true) {
				x = x*PI/180;
				
				
			}
			int n;
			double val=0;
			for (n=0;n<8;n++)
			{
				double p = power(-1,n);
				double px = power(x,2*n);
				long fac = factorial(2*n);
				val += p * px / fac;
			}
			return val;
		}
		catch(ArithmeticException e) {
			System.out.println("The Value Entered is Incorrect");
			System.out.println("**************************************");
			e.printStackTrace();
		}

		return 0;
	}
	/**
	 * @param x is either radian or degree from which we want to use tan function on
	 * @param is_degree determines whether x is given in radian or degree
	 * @return returns the value of tan function given the parameters
	 */
	public static double tan(double x, boolean is_degree) {
		double p = sin(x, is_degree);
		double q = cos(x, is_degree);
		double result = p/q;
		return result;

	}
	/**
	 * @param a first integer
	 * @param b second integer
	 * @return the maximum of 2 input parameters
	 */
	public static final int max ( final int a, final int b )
	{
		if ( a > b )
		{
			return a;
		}

		return b;
	}


}