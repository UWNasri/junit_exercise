package com.example._math;
import org.junit.runner.Result;		

import org.junit.runner.JUnitCore;		
import org.junit.runner.notification.Failure;		
		

public class MathematTestRunner 
{

	public static void main(String[] args) 
	{
		 Result result = JUnitCore.runClasses(MathematTest.class);		
		 
			for (Failure failure : result.getFailures()) 					
				System.out.println(failure.toString());					
  	
			System.out.println("Result=="+result.wasSuccessful());
			//System.out.println(Math.sin(Math.toRadians(180)) + "\n");
	}

}
